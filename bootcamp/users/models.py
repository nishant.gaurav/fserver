import uuid

from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.signals import user_logged_in, user_logged_out
from django.contrib.gis.db import models
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from django.contrib.gis.geos import Point

from bootcamp.notifications.models import Notification, notification_handler

class City(models.Model):
    name = models.CharField(_('City'), max_length=100, null=False, blank=False)

    def __str__(self):
        return self.name

class School(models.Model):
    name = models.CharField(_('Class'), max_length=100, null=False, blank=False)

    def __str__(self):
        return self.name

class Phone(models.Model):
    number = models.CharField(_(''), max_length=20, blank=False, null=False, default="", unique=True)
    country_prefix = models.CharField(_(''), max_length=5, blank=False, null=False, default="")
    national_number = models.CharField(_(''), max_length=15, blank=False, null=False, default="")
    is_whatsapp_verified = models.BooleanField(blank=True, default=False)
    name = models.CharField(_("User's name"), blank=True, max_length=255)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, on_delete=models.SET_NULL)

    def save(self, *args, **kwargs):
        if not self.name:
            self.name = self.user.name
        super().save(*args, **kwargs)

SEX_CHOICES = (
    ('F', 'Female',),
    ('M', 'Male',),
    ('X', 'Not Specified',),
)

class UserImage(models.Model):
    uuid = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=False, on_delete=models.CASCADE)
    rank = models.IntegerField(blank=False, null=False, default=0)
    image = models.ImageField(upload_to='user_pics/')

    class Meta:
        ordering = ['rank']

class User(AbstractUser):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    # First Name and Last Name do not cover name patterns around the globe.
    name = models.CharField(_("User's name"), blank=True, default="", null=False, max_length=255)
    primary_phone = models.ForeignKey(Phone, null=True, blank=True, on_delete=models.SET_NULL, related_name="primary_phone_of")
    picture = models.ForeignKey(UserImage, null=True, blank=True, on_delete=models.SET_NULL, related_name="dp_of")
    city = models.ForeignKey(City, null=True, blank=True, on_delete=models.SET_NULL)
    school = models.ForeignKey(School, null=True, blank=True, on_delete=models.SET_NULL, related_name="student")
    birth_date = models.DateField(blank=True, null=True)
    sex = models.CharField(max_length=1, choices=SEX_CHOICES, db_index=True, blank=False, null=True, default='X')
    last_location = models.PointField(max_length=40, blank=True, null=True, default=Point(-157.8583,21.3069))
    bio = models.CharField(_("Short bio"), max_length=280, blank=True, null=False, default="")
    is_online = models.BooleanField(blank=True, default=False)
    is_new = models.BooleanField(blank=True, default=False)
    show_me = models.BooleanField(blank=True, default=True)
    invited_by = models.ForeignKey('self', null=True, blank=True, on_delete=models.SET_NULL)

    def __str__(self):
        return self.username

    class Meta(object):
        unique_together = ('email',)

    @property
    def avatar(self):
        if self.picture:
            return self.picture.image.url
        if self.userimage_set.first():
            return self.userimage_set.first().image.url
        return ""
    
    @property
    def phone(self):
        if self.primary_phone:
            return self.primary_phone.number
        if self.phone_set.first():
            return self.phone_set.first().number
        return ""

    def get_absolute_url(self):
        return reverse("users:detail", kwargs={"username": self.username})

    def get_profile_name(self):
        if self.name:
            return self.name

        return self.username

    @property
    def invite_code(self):
        return self.username

    @property
    def invite_count(self):
        return User.objects.filter(invited_by=self).count()


def broadcast_login(sender, user, request, **kwargs):
    """Handler to be fired up upon user login signal to notify all users."""
    notification_handler(user, "global", Notification.LOGGED_IN)


def broadcast_logout(sender, user, request, **kwargs):
    """Handler to be fired up upon user logout signal to notify all users."""
    notification_handler(user, "global", Notification.LOGGED_OUT)


# user_logged_in.connect(broadcast_login)
# user_logged_out.connect(broadcast_logout)

# FCM
class UserRegistrationTokenManager(models.Manager):

    def create_and_set_user_token(self, user, token):
        object, created = UserRegistrationToken.objects.get_or_create(user=user)
        object.registration_token = token
        object.save()

        return object

    def delete_user_token(self, user):
        try:
            UserRegistrationToken.objects.get(user=user).delete()
        except UserRegistrationToken.DoesNotExist:
            pass

    def get_token_if_exists(self, user):
        try:
            return UserRegistrationToken.objects.get(user=user).registration_token
        except UserRegistrationToken.DoesNotExist:
            return None


class UserRegistrationToken(models.Model):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        to_field='id',
        primary_key=True)
    registration_token = models.CharField(max_length=250, null=False, default="")
    created = models.DateTimeField(auto_now_add=True)

    objects = UserRegistrationTokenManager()
