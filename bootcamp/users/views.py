import json

from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse
from django.views.generic import DetailView, ListView, RedirectView, UpdateView
from django.contrib.gis.geos import Point
from knox.views import LogoutView, LogoutAllView
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status, permissions

from .models import User, UserRegistrationToken
from .serializers import UserSerializer

class UserDetailView(LoginRequiredMixin, DetailView):
    model = User
    # These next two lines tell the view to index lookups by username
    slug_field = "username"
    slug_url_kwarg = "username"


class UserRedirectView(LoginRequiredMixin, RedirectView):
    permanent = False

    def get_redirect_url(self):
        return reverse("users:detail", kwargs={"username": self.request.user.username})


# class UserUpdateView(LoginRequiredMixin, UpdateView):
#     fields = [
#         "name",
#         "email",
#         "picture",
#         "job_title",
#         "location",
#         "personal_url",
#         "facebook_account",
#         "twitter_account",
#         "github_account",
#         "linkedin_account",
#         "short_bio",
#         "bio",
#     ]
#     model = User

#     # send the user back to their own page after a successful update
#     def get_success_url(self):
#         return reverse("users:detail", kwargs={"username": self.request.user.username})

#     def get_object(self):
#         # Only get the User record for the user making the request
#         return User.objects.get(username=self.request.user.username)


class UserListView(LoginRequiredMixin, ListView):
    model = User
    # These next two lines tell the view to index lookups by username
    slug_field = "username"
    slug_url_kwarg = "username"


class UserValidationView(APIView):

    def get(self, request):
        return Response(UserSerializer(request.user).data)


class FCMLogoutView(LogoutView):
    def post(self, request):
        UserRegistrationToken.objects.delete_user_token(request.user)
        return super(FCMLogoutView, self).post(request, format=None)


class FCMLogoutAllView(LogoutAllView):
    def post(self, request):
        UserRegistrationToken.objects.delete_user_token(request.user)
        return super(FCMLogoutAllView, self).post(request, format=None)


class UpdateLocationView(APIView):

    def post(self, request):
        lat = json.loads(request.POST.get("latitude"))
        lon = json.loads(request.POST.get("longitude"))
        if (lat>90 or lat<-90 or lon>180 or lon<-180):
            return Response(status=status.HTTP_406_NOT_ACCEPTABLE)
        request.user.last_location = Point(lon,lat)
        request.user.save()
        return Response(status=status.HTTP_202_ACCEPTED)


class RegistrationToken(APIView):

    def post(self, request, *args, **kwargs):
        self.check_object_permissions(request, None)

        token = request.data.get('token', None)
        if token is None:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        elif token == "":
            return Response(status=status.HTTP_400_BAD_REQUEST)

        if request.user.is_new:
            request.user.is_new = False
        request.user.save()

        UserRegistrationToken.objects.create_and_set_user_token(request.user, token)
        return Response(status=status.HTTP_201_CREATED)

    def delete(self, request, *args, **kwargs):
        self.check_object_permissions(request, None)

        UserRegistrationToken.objects.delete_user_token(request.user)
        return Response(status=status.HTTP_200_OK)
