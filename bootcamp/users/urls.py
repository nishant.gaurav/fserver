from django.conf.urls import url

from . import views

app_name = "users"
urlpatterns = [
    url(regex=r"^$", view=views.UserListView.as_view(), name="list"),
    url(regex=r"^~redirect/$", view=views.UserRedirectView.as_view(), name="redirect"),
    # url(regex=r"^~update/$", view=views.UserUpdateView.as_view(), name="update"),
    url(
        regex=r"^(?P<username>[\w.@+-]+)/$",
        view=views.UserDetailView.as_view(),
        name="detail",
    ),
    url(
        regex=r'^~validate/$',
        view=views.UserValidationView.as_view(),
        name='validate'
    ),
    url(
        regex=r'^~logout/$',
        view=views.FCMLogoutView.as_view(),
        name='fcm_logout'
    ),
    url(
        regex=r'^~logoutall/$',
        view=views.FCMLogoutAllView.as_view(),
        name='fcm_logoutall'
    ),
    url(
        regex=r'^~location/$',
        view=views.UpdateLocationView.as_view(),
        name='update_location'
    ),
    url(
        regex=r'^~fcmtoken/$',
        view=views.RegistrationToken.as_view(),
        name='token-detail'
    ),
]
