from rest_framework import serializers

from bootcamp.users.models import User, UserImage, SEX_CHOICES, School, City, Phone, UserRegistrationToken

from django.contrib.gis.geos import fromstr

class UserImageSerializer(serializers.ModelSerializer):
    url = serializers.CharField(source='image.url', read_only=True)
    class Meta:
        model = UserImage
        fields = ('url','uuid')

class SchoolSerializer(serializers.ModelSerializer):
    class Meta:
        model = School
        fields = '__all__'

class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = '__all__'

class PhoneSerializer(serializers.ModelSerializer):
    class Meta:
        model = Phone
        fields = '__all__'

user_common_fields = ('id','name','avatar','images','sex','birth_date','bio','school','city','is_active','is_new','show_me')

class UserSerializer(serializers.ModelSerializer):
    sex = serializers.ChoiceField(choices=SEX_CHOICES, default='Male')
    images = UserImageSerializer(source='userimage_set', many=True, read_only=True)
    school = serializers.CharField(source='school.name', default="", read_only=True)
    city = serializers.CharField(source='city.name', default="", read_only=True)

    class Meta:
        model = User
        fields = (*user_common_fields,)

class UserListSerializer(serializers.ListSerializer):
    child = UserSerializer()
    many = True
    allow_null = True

# FCM
class UserRegistrationTokenSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    registration_token = serializers.ReadOnlyField(source='registration_token')

    class Meta:
        model = UserRegistrationToken
        fields = ('user', 'registration_token')