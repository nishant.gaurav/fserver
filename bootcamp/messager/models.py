import uuid

from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone

from bootcamp.messager.fcm import send_event_via_fcm

from bootcamp.users.signals import user_created

class MessageThread(models.Model):
    """Thread for messages

    Consists of:
     - uuid_id
     - ManyToManyField containing clients participating in the thread
     - ForeignKey last_message to last Message in thread
    Brief:
     - mark_read marks all messages read for a particular user
     - add_message_text adds a message sent by sender
    """
    uuid_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    title = models.CharField(max_length=64, default="", null=False, blank=True)
    timestamp = models.DateTimeField(blank=True, auto_now_add=True)
    clients = models.ManyToManyField(settings.AUTH_USER_MODEL,blank=True,through='Membership')
    last_message = models.ForeignKey('messager.Message',null=True,blank=True,on_delete=models.SET_NULL)
    is_active = models.BooleanField(blank=True, default=True)
    last_activity = models.DateTimeField(blank=True, auto_now_add=True)

    def mark_read(self,user):
        UnreadReceipt.objects.filter(recipient=user,thread=self).delete()

    def add_message_text(self,text,sender):
        """User sends text to the chat
         - creates new message with foreign key to self
         - adds unread receipt for each user
         - returns instance of new message
        """
        new_message = Message(text=text,sender=sender,thread=self)
        new_message.save()
        self.last_message = new_message
        self.last_activity = timezone.now()
        self.save()
        return new_message

    class Meta:
        ordering = ("-last_activity", )

class Membership(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    messagethread = models.ForeignKey(MessageThread, on_delete=models.CASCADE)
    notification_on = models.BooleanField(blank=True, default=True)
    archived = models.BooleanField(blank=True, default=False)

class Message(models.Model):
    """Thread Message

    Consists of:
     - id 1 to 9.223.372.036.854.775.807
     - timestamp
     - TextField for text message, it may be empty, avoid two possible values for “no data”, that is: None and an empty string
     - ForeignKey to sender, it may be null for the time when user is deleted.
     - ForeignKey to thread it was sent to
    """
    id = models.BigAutoField(primary_key=True)
    timestamp = models.DateTimeField(auto_now_add=True)
    text = models.TextField(blank=True, null=False, default="")
    sender = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name='sent_messages',
        verbose_name=_("Sender"), null=True, on_delete=models.SET_NULL)
    thread = models.ForeignKey('messager.MessageThread', related_name='messages',
        verbose_name=_("Thread"), on_delete=models.CASCADE)

    class Meta:
        verbose_name = _("Message")
        verbose_name_plural = _("Messages")
        ordering = ("-timestamp", )

    def __str__(self):
        return self.text


class UnreadReceipt(models.Model):
    """Unread receipt for unread messages

    Consists of:
     - timestamp
     - ForeignKey to corresponding Message
     - ForeignKey to Thread
     - ForeignKey to User who has not yet seen message

    Brief:
     - deleted when a user loads thread messages or when they respond with
       the 'read' message over websocket connection
    """
    timestamp = models.DateTimeField(auto_now_add=True)
    message = models.ForeignKey('messager.Message',on_delete=models.CASCADE,related_name='receipts')
    thread = models.ForeignKey('messager.MessageThread',on_delete=models.CASCADE,related_name='receipts')
    recipient = models.ForeignKey(settings.AUTH_USER_MODEL,on_delete=models.CASCADE,related_name='receipts')

def add_chatroom(**kwargs):
    """Handler to be fired up when a freindship request is accepted"""
    from_user = kwargs['from_user']
    to_user = kwargs['to_user']
    title = from_user.username + to_user.username
    if (from_user.username > to_user.username):
        title = to_user.username + from_user.username
    thread, _ = MessageThread.objects.get_or_create(title=title)
    if not from_user in thread.clients.all():
        thread.clients.add(from_user)
    if not to_user in thread.clients.all():
        thread.clients.add(to_user)
    thread.save()
    return thread

def welcome_users(**kwargs):
    thread = add_chatroom(**kwargs)
    TEXT = "Hi " + str(kwargs['to_user'].name) + " :) Welcome to the club!"
    thread.add_message_text(TEXT,kwargs['from_user'])
    notification_serialized = {
                        "message_body":"new application recieved",
                        "thread_id":str(thread.uuid_id),
                        "peer_display_name":str(kwargs['to_user'].name),
                        "peer_id":str(kwargs['to_user'].id),
                        "peer_avatar":str(kwargs['to_user'].avatar),
                    }
    send_event_via_fcm(kwargs['from_user'], notification_serialized)


# user_created.connect(receiver=welcome_users)