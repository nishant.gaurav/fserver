from django.db.models import fields
from rest_framework import serializers

from . import models
from quiz.models import Course

class PlanCostSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.PlanCost
        fields = '__all__'

class SubscriptionPlanSerializer(serializers.ModelSerializer):
    plancosts = PlanCostSerializer(source='plancost_set', many=True, read_only=True)
    class Meta:
        model = models.SubscriptionPlan
        fields = '__all__'

class SubscriptionPlanListSerializer(serializers.ListSerializer):
    child = SubscriptionPlanSerializer()
    many = True
    allow_null = True
