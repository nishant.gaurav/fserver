from django.conf.urls import url

from . import views

app_name = 'subscription'
urlpatterns = [
    url(r'^plans/$', views.LoadCourses.as_view(), name='plans'),
    url(r'^plans/(?P<course>[\w-]+)/$', views.LoadSubscriptionPlan.as_view(), name='plan'),
]
