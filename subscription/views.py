import json

from . import models
from .serializers import SubscriptionPlanListSerializer
from django.shortcuts import get_object_or_404

from quiz.models import Course
from quiz.serializers import CourseListSerializer, CourseSerializer

from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status, permissions

class LoadCourses(APIView):

    def get(self, request):
        courses = Course.objects.filter(active=True)
        courses_data = CourseListSerializer(courses).data
        return Response(courses_data)

class LoadSubscriptionPlan(APIView):

    def get(self, request, course):
        course = get_object_or_404(Course, url=course)
        subscription_plans = course.group.plans.all()
        plans_data = SubscriptionPlanListSerializer(subscription_plans).data
        return Response({'course':CourseSerializer(course).data, 'plans':plans_data})
