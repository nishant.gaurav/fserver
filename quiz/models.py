from __future__ import unicode_literals
import re
import json

from django.db import models
from django.core.exceptions import ValidationError, ImproperlyConfigured
from django.core.validators import (
    MaxValueValidator, validate_comma_separated_integer_list, validate_unicode_slug
)
from django.utils.translation import ugettext_lazy as _
from django.utils.timezone import now
from django.conf import settings
from django.template import Template, Context
from django.contrib.postgres.fields import ArrayField
from django.contrib.auth.models import Group

from taggit.managers import TaggableManager
from markdownx.models import MarkdownxField
from markdownx.utils import markdownify

class CategoryManager(models.Manager):

    def new_category(self, category):
        new_category = self.create(category=re.sub('\s+', '-', category)
                                   .lower())

        new_category.save()
        return new_category


class Category(models.Model):

    category = models.CharField(
        verbose_name=_("Category"),
        max_length=250, blank=True,
        unique=True, null=True)

    objects = CategoryManager()

    class Meta:
        verbose_name = _("Category")
        verbose_name_plural = _("Categories")

    def __str__(self):
        return self.category


class SubCategory(models.Model):

    sub_category = models.CharField(
        verbose_name=_("Sub-Category"),
        max_length=250, blank=True, null=True)

    category = models.ForeignKey(
        Category, null=True, blank=True,
        verbose_name=_("Category"), on_delete=models.CASCADE)

    objects = CategoryManager()

    class Meta:
        verbose_name = _("Sub-Category")
        verbose_name_plural = _("Sub-Categories")

    def __str__(self):
        return self.sub_category + " (" + self.category.category + ")"

CLASS_LEVEL = (
    (0, _('Class D (or 9th)')),
    (1, _('Class C (or 10th)')),
    (2, _('Class B (or 11th)')),
    (3, _('Class A (or 12th)')),
    (4, _('Class S (or 12+)')),
)

class Quiz(models.Model):

    title = models.CharField(
        verbose_name=_("Title"),
        max_length=60, blank=False)

    description = models.TextField(
        verbose_name=_("Description"),
        blank=True, help_text=_("a description of the quiz"))

    level = models.PositiveIntegerField(
        choices=CLASS_LEVEL, blank=False,
        help_text=_("Level of this quiz or the simply the grade"),
        verbose_name=_("Class Level"))

    url = models.SlugField(
        max_length=60, blank=False,
        help_text=_("a user friendly url"),
        verbose_name=_("user friendly url"))

    category = models.ForeignKey(
        Category, null=True, blank=True,
        verbose_name=_("Category"), on_delete=models.CASCADE)

    random_order = models.BooleanField(
        blank=False, default=False,
        verbose_name=_("Random Order"),
        help_text=_("Display the questions in "
                    "a random order or as they "
                    "are set?"))

    question_order = models.CharField(
        max_length=1024, default="", blank=True,
        verbose_name=_("Question Order"),
        validators=[validate_comma_separated_integer_list])

    max_questions = models.PositiveIntegerField(
        blank=True, null=True, verbose_name=_("Max Questions"),
        help_text=_("Number of questions to be answered on each attempt."))

    answers_at_end = models.BooleanField(
        blank=False, default=False,
        help_text=_("Correct answer is NOT shown after question."
                    " Answers displayed at the end."),
        verbose_name=_("Answers at end"))

    exam_paper = models.BooleanField(
        blank=False, default=False,
        help_text=_("If yes, the result of each"
                    " attempt by a user will be"
                    " stored. Necessary for marking."),
        verbose_name=_("Exam Paper"))

    single_attempt = models.BooleanField(
        blank=False, default=False,
        help_text=_("If yes, only one attempt by"
                    " a user will be permitted."
                    " Non users cannot sit this exam."),
        verbose_name=_("Single Attempt"))

    pass_mark = models.SmallIntegerField(
        blank=True, default=0,
        verbose_name=_("Pass Mark"),
        help_text=_("Percentage required to pass exam."),
        validators=[MaxValueValidator(100)])

    success_text = models.TextField(
        blank=True, help_text=_("Displayed if user passes."),
        verbose_name=_("Success Text"))

    fail_text = models.TextField(
        verbose_name=_("Fail Text"),
        blank=True, help_text=_("Displayed if user fails."))

    draft = models.BooleanField(
        blank=True, default=False,
        verbose_name=_("Draft"),
        help_text=_("If yes, the quiz is not displayed"
                    " in the quiz list and can only be"
                    " taken by users who can edit"
                    " quizzes."))

    prerequisites = models.ManyToManyField(
        "self",
        through='Prerequisite',
        through_fields=('dependent', 'dependency'),
    )

    def save(self, force_insert=False, force_update=False, *args, **kwargs):
        self.url = re.sub('\s+', '-', self.url).lower()

        self.url = ''.join(letter for letter in self.url if
                           letter.isalnum() or letter == '-')

        if self.single_attempt is True:
            self.exam_paper = True

        if self.pass_mark > 100:
            raise ValidationError('%s is above 100' % self.pass_mark)

        super(Quiz, self).save(force_insert, force_update, *args, **kwargs)

    class Meta:
        verbose_name = _("Quiz")
        verbose_name_plural = _("Quizzes")

    def __str__(self):
        return self.title

    def get_questions(self):
        return Question.objects.filter(container__quiz=self.id)

    @property
    def get_max_score(self):
        return self.get_questions().count()

    def anon_score_id(self):
        return str(self.id) + "_score"

    def anon_q_list(self):
        return str(self.id) + "_q_list"

    def anon_q_data(self):
        return str(self.id) + "_data"

class Prerequisite(models.Model):
    dependent = models.ForeignKey(Quiz, on_delete=models.CASCADE, related_name="prerequisites_dependent")
    dependency = models.ForeignKey(Quiz, on_delete=models.CASCADE, related_name="prerequisites_dependency",)

class SessionManager(models.Manager):
    def new_session(self, user, quiz):
        if quiz.random_order is True:
            question_set = quiz.get_questions().order_by('?')
        else:
            question_set = quiz.get_questions()

        question_set = [item.id for item in question_set]

        if len(question_set) == 0:
            raise ImproperlyConfigured('Question set of the quiz is empty. '
                                       'Please configure questions properly')

        if quiz.max_questions and quiz.max_questions < len(question_set):
            question_set = question_set[:quiz.max_questions]

        new_session = self.create(user=user,
                                  quiz=quiz,
                                  questions=question_set,
                                  complete=False)
        return new_session

    def user_session(self, user, quiz):
        if quiz.single_attempt is True and self.filter(user=user, quiz=quiz, complete=True).exists():
            return False
        try:
            session = self.get(user=user, quiz=quiz, complete=False)
        except Session.DoesNotExist:
            session = self.new_session(user, quiz)
        except Session.MultipleObjectsReturned:
            session = self.filter(user=user, quiz=quiz, complete=False)[0]
        return session

class Session(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_("User"), on_delete=models.CASCADE)
    quiz = models.ForeignKey(Quiz, verbose_name=_("Quiz"), on_delete=models.CASCADE)
    questions = ArrayField(models.IntegerField(), verbose_name=_("Question Order"), null=True, blank=True)
    complete = models.BooleanField(default=False, blank=False, verbose_name=_("Complete"))
    objects = SessionManager()

class ProgressManager(models.Manager):

    def new_progress(self, user):
        new_progress = self.create(user=user,
                                   score="")
        new_progress.save()
        return new_progress


class Progress(models.Model):
    """
    Progress is used to track an individual signed in users score on different
    quiz's and categories
    Data stored in csv using the format:
        category, score, possible, category, score, possible, ...
    """
    user = models.OneToOneField(settings.AUTH_USER_MODEL, verbose_name=_("User"), on_delete=models.CASCADE)

    score = models.CharField(max_length=1024,
                             verbose_name=_("Score"),
                             validators=[validate_comma_separated_integer_list])

    objects = ProgressManager()

    class Meta:
        verbose_name = _("User Progress")
        verbose_name_plural = _("User progress records")

    @property
    def list_all_cat_scores(self):
        """
        Returns a dict in which the key is the category name and the item is
        a list of three integers.
        The first is the number of questions correct,
        the second is the possible best score,
        the third is the percentage correct.
        The dict will have one key for every category that you have defined
        """
        score_before = self.score
        output = {}

        for cat in Category.objects.all():
            to_find = re.escape(cat.category) + r",(\d+),(\d+),"
            #  group 1 is score, group 2 is highest possible

            match = re.search(to_find, self.score, re.IGNORECASE)

            if match:
                score = int(match.group(1))
                possible = int(match.group(2))

                try:
                    percent = int(round((float(score) / float(possible))
                                        * 100))
                except:
                    percent = 0

                output[cat.category] = [score, possible, percent]

            else:  # if category has not been added yet, add it.
                self.score += cat.category + ",0,0,"
                output[cat.category] = [0, 0]

        if len(self.score) > len(score_before):
            # If a new category has been added, save changes.
            self.save()

        return output

    def update_score(self, question, score_to_add=0, possible_to_add=0):
        """
        Pass in question object, amount to increase score
        and max possible.
        Does not return anything.
        """
        category_test = Category.objects.filter(category=question.category)\
                                        .exists()

        if any([item is False for item in [category_test,
                                           score_to_add,
                                           possible_to_add,
                                           isinstance(score_to_add, int),
                                           isinstance(possible_to_add, int)]]):
            return _("error"), _("category does not exist or invalid score")

        to_find = re.escape(str(question.category)) +\
            r",(?P<score>\d+),(?P<possible>\d+),"

        match = re.search(to_find, self.score, re.IGNORECASE)

        if match:
            updated_score = int(match.group('score')) + abs(score_to_add)
            updated_possible = int(match.group('possible')) +\
                abs(possible_to_add)

            new_score = ",".join(
                [
                    str(question.category),
                    str(updated_score),
                    str(updated_possible), ""
                ])

            # swap old score for the new one
            self.score = self.score.replace(match.group(), new_score)
            self.save()

        else:
            #  if not present but existing, add with the points passed in
            self.score += ",".join(
                [
                    str(question.category),
                    str(score_to_add),
                    str(possible_to_add),
                    ""
                ])
            self.save()

    def show_exams(self):
        """
        Finds the previous quizzes marked as 'exam papers'.
        Returns a queryset of complete exams.
        """
        return Sitting.objects.filter(user=self.user, complete=True)


class SittingManager(models.Manager):

    def new_sitting(self, user, quiz):
        if quiz.random_order is True:
            question_set = quiz.get_questions().order_by('?')
        else:
            question_set = quiz.get_questions()

        question_set = [item.id for item in question_set]

        if len(question_set) == 0:
            raise ImproperlyConfigured('Question set of the quiz is empty. '
                                       'Please configure questions properly')

        if quiz.max_questions and quiz.max_questions < len(question_set):
            question_set = question_set[:quiz.max_questions]

        questions = ",".join(map(str, question_set)) + ","

        new_sitting = self.create(user=user,
                                  quiz=quiz,
                                  question_order=questions,
                                  question_list=questions,
                                  incorrect_questions="",
                                  current_score=0,
                                  complete=False,
                                  user_answers='{}')
        return new_sitting

    def user_sitting(self, user, quiz):
        if quiz.single_attempt is True and self.filter(user=user,
                                                       quiz=quiz,
                                                       complete=True)\
                                               .exists():
            return False

        try:
            sitting = self.get(user=user, quiz=quiz, complete=False)
        except Sitting.DoesNotExist:
            sitting = self.new_sitting(user, quiz)
        except Sitting.MultipleObjectsReturned:
            sitting = self.filter(user=user, quiz=quiz, complete=False)[0]
        return sitting

class Sitting(models.Model):
    """
    Used to store the progress of logged in users sitting a quiz.
    Replaces the session system used by anon users.
    Question_order is a list of integer pks of all the questions in the
    quiz, in order.
    Question_list is a list of integers which represent id's of
    the unanswered questions in csv format.
    Incorrect_questions is a list in the same format.
    Sitting deleted when quiz finished unless quiz.exam_paper is true.
    User_answers is a json object in which the question PK is stored
    with the answer the user gave.
    """

    user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_("User"), on_delete=models.CASCADE)

    quiz = models.ForeignKey(Quiz, verbose_name=_("Quiz"), on_delete=models.CASCADE)

    question_order = models.CharField(
        max_length=1024,
        verbose_name=_("Question Order"),
        validators=[validate_comma_separated_integer_list])

    question_list = models.CharField(
        max_length=1024,
        verbose_name=_("Question List"),
        validators=[validate_comma_separated_integer_list])

    incorrect_questions = models.CharField(
        max_length=1024,
        blank=True,
        verbose_name=_("Incorrect questions"),
        validators=[validate_comma_separated_integer_list])

    current_score = models.IntegerField(verbose_name=_("Current Score"))

    complete = models.BooleanField(default=False, blank=False,
                                   verbose_name=_("Complete"))

    user_answers = models.TextField(blank=True, default='{}',
                                    verbose_name=_("User Answers"))

    start = models.DateTimeField(auto_now_add=True,
                                 verbose_name=_("Start"))

    end = models.DateTimeField(null=True, blank=True, verbose_name=_("End"))

    objects = SittingManager()

    class Meta:
        permissions = (("view_sittings", _("Can see completed exams.")),)

    def get_first_question(self):
        """
        Returns the next question.
        If no question is found, returns False
        Does NOT remove the question from the front of the list.
        """
        if not self.question_list:
            return False

        first, _ = self.question_list.split(',', 1)
        question_id = int(first)
        return Question.objects.get_subclass(id=question_id)

    def remove_first_question(self):
        if not self.question_list:
            return

        _, others = self.question_list.split(',', 1)
        self.question_list = others
        self.save()

    def add_to_score(self, points):
        self.current_score += int(points)
        self.save()

    @property
    def get_current_score(self):
        return self.current_score

    def _question_ids(self):
        return [int(n) for n in self.question_order.split(',') if n]

    @property
    def get_percent_correct(self):
        dividend = float(self.current_score)
        divisor = len(self._question_ids())
        if divisor < 1:
            return 0            # prevent divide by zero error

        if dividend > divisor:
            return 100

        correct = int(round((dividend / divisor) * 100))

        if correct >= 1:
            return correct
        else:
            return 0

    def mark_quiz_complete(self):
        self.complete = True
        self.end = now()
        self.save()

    def add_incorrect_question(self, question):
        """
        Adds uid of incorrect question to the list.
        The question object must be passed in.
        """
        if len(self.incorrect_questions) > 0:
            self.incorrect_questions += ','
        self.incorrect_questions += str(question.id) + ","
        if self.complete:
            self.add_to_score(-1)
        self.save()

    @property
    def get_incorrect_questions(self):
        """
        Returns a list of non empty integers, representing the pk of
        questions
        """
        return [int(q) for q in self.incorrect_questions.split(',') if q]

    def remove_incorrect_question(self, question):
        current = self.get_incorrect_questions
        current.remove(question.id)
        self.incorrect_questions = ','.join(map(str, current))
        self.add_to_score(1)
        self.save()

    @property
    def check_if_passed(self):
        return self.get_percent_correct >= self.quiz.pass_mark

    @property
    def result_message(self):
        if self.check_if_passed:
            return self.quiz.success_text
        else:
            return self.quiz.fail_text

    def add_user_answer(self, question, guess):
        current = json.loads(self.user_answers)
        current[question.id] = guess
        self.user_answers = json.dumps(current)
        self.save()

    def get_questions(self, with_answers=False):
        question_ids = self._question_ids()
        questions = sorted(
            self.quiz.get_questions().filter(id__in=question_ids),
            key=lambda q: question_ids.index(q.id))

        if with_answers:
            user_answers = json.loads(self.user_answers)
            for question in questions:
                question.user_answer = user_answers[str(question.id)]

        return questions

    @property
    def questions_with_user_answers(self):
        return {
            q: q.user_answer for q in self.get_questions(with_answers=True)
        }

    @property
    def get_max_score(self):
        return len(self._question_ids())

    def progress(self):
        """
        Returns the number of questions answered so far and the total number of
        questions.
        """
        answered = len(json.loads(self.user_answers))
        total = self.get_max_score
        return answered, total


class Container(models.Model):
    quiz = models.ManyToManyField(Quiz,
                                  verbose_name=_("Quiz"),
                                  blank=True)

    paragraph = MarkdownxField(blank=True, null=False, default='',
                            help_text=_("Enter the paragraph on which "
                                        "the questions are based."),
                            verbose_name=_('Paragraph'))

    category = models.ForeignKey(Category,
                                 verbose_name=_("Category"),
                                 blank=True,
                                 null=True,
                                 on_delete=models.CASCADE)

    sub_category = models.ForeignKey(SubCategory,
                                     verbose_name=_("Sub-Category"),
                                     blank=True,
                                     null=True,
                                     on_delete=models.CASCADE)

    @property
    def get_paragraph(self):
        return markdownify(self.paragraph)

    def __str__(self):
        try:
            return "ID: " + str(self.id) + "; " + self.question_set.first().content[:50]
        except:
            return "ID: " + str(self.id)


ANSWER_ORDER_OPTIONS = (
    ('content', _('Content')),
    ('random', _('Random')),
    ('none', _('None'))
)


QUESTION_TYPES = (
    ('MLCH', _('Multiple Choice')),
    ('TITA', _('Type In The Answer')),
)


DIFFICULTY_LEVEL = (
    (1, _('Basic')),
    (2, _('Standard but calculative')),
    (3, _('Trap for noobs or Unethical')),
    (4, _('Multiple concepts or Advanced')),
    (5, _('Should not be attempted')),
)

class Question(models.Model):
    """
    Base class for all question types.
    Shared properties placed here.
    """

    updated_at = models.DateTimeField(auto_now_add=True,
                                      verbose_name=_("Last Updated"))

    global_variables = models.TextField(
                                    blank=True, null=False, default='',
                                    help_text=_("Define the global variables "
                                                "like python dict."),
                                    verbose_name=_('Global Variables'))

    container = models.ForeignKey(Container,
                                  blank=True, null=False,
                                  help_text=_("Allows for common paragraphs in "
                                              "paragraph based questions."),
                                  verbose_name=_("Container"),
                                  on_delete=models.PROTECT)

    content = MarkdownxField(blank=False,
                             help_text=_("Enter the question text that "
                                         "you want displayed"),
                             verbose_name=_('Question'))

    @property
    def get_content(self):
        # template = Template(self.content)
        # j = json.loads(self.global_variables)
        # context = Context(j[random.randrange(len(j))])
        # return markdownify(template.render(context))
        return markdownify(self.content)

    @property
    def get_raw_content(self):
        return self.content.replace("$`","<tex>").replace("`$","</tex>")

    explanation = MarkdownxField(blank=False,
                                 help_text=_("Explanation to be shown "
                                             "after the question has "
                                             "been answered."),
                                 verbose_name=_('Explanation'))

    @property
    def get_explanation(self):
        return markdownify(self.explanation)

    hint = MarkdownxField(blank=True, null=False, default='',
                          help_text=_("Hint to be shown "
                                      "with the question. "
                                      "Try to use colors."),
                          verbose_name=_('Hint'))

    @property
    def get_hint(self):
        return markdownify(self.hint)

    instructions = models.CharField(max_length=250,
                                    blank=True, null=False, default='',
                                    help_text=_("Enter any instructions "
                                                "to show before the question."),
                                    verbose_name=_('Optional Instructions'))

    question_help = models.TextField(blank=True, null=False, default='',
                                    help_text=_("Enter any instructions "
                                                "to show with the question."),
                                    verbose_name=_('Optional Help'))

    tags = TaggableManager()

    question_type = models.CharField(
        max_length=4, null=False, blank=False,
        choices=QUESTION_TYPES, default='MLCH',
        help_text=_("If the question is MCQ "
                    "or essay type."),
        verbose_name=_("Answer Type"))

    level = models.PositiveIntegerField(
        choices=DIFFICULTY_LEVEL, blank=False,
        help_text=_("How difficult the "
                    "question is?"),
        verbose_name=_("Difficulty Level"))

    answer_text = models.CharField(
        max_length=8, null=False, blank=True,
        default='', validators=[validate_unicode_slug],
        help_text=_("The answer without any "
                    "spaces. Leave it blank "
                    "for MCQ type questions."),
        verbose_name=_("Correct Answer Text"))

    answer_order = models.CharField(
        max_length=30, null=False, blank=True,
        choices=ANSWER_ORDER_OPTIONS, default='random',
        help_text=_("The order in which multichoice "
                    "answer options are displayed "
                    "to the user"),
        verbose_name=_("Answer Order"))

    def check_if_correct(self, guesses):
        if len(guesses)>0:
            if self.question_type == 'MLCH':
                answers = [answer.id for answer in Answer.objects.filter(question=self, correct=True)]
                answers.sort()
                guesses.sort()
                return answers == guesses
            elif self.question_type == 'TITA':
                return (str(self.answer_text).upper() == guesses.upper())
        else:
            return False

    def order_answers(self, queryset):
        if self.answer_order == 'content':
            return queryset.order_by('content')
        if self.answer_order == 'random':
            return queryset.order_by('?')
        if self.answer_order == 'none':
            return queryset.order_by()
        return queryset

    def get_answers(self):
        if self.question_type == 'MLCH':
            return self.order_answers(Answer.objects.filter(question=self))
        elif self.question_type == 'TITA':
            return str(self.answer_text).upper()
        else:
            return False

    @property
    def category(self):
        return self.container.category

    @property
    def sub_category(self):
        return self.container.sub_category

    @property
    def is_multi_correct(self):
        return Answer.objects.filter(question=self).filter(correct=True).count() > 1

    def get_answers_list(self):#for MLCH type questions
        return [(answer.id, answer.content) for answer in
                self.order_answers(Answer.objects.filter(question=self))]

    def answer_choice_to_string(self, guess):
        if self.question_type == 'MLCH':
            return Answer.objects.get(id=guess).content
        elif self.question_type == 'TITA':
            str(guess)
        else:
            return "answer_choice_to_string error"

    class Meta:
        verbose_name = _("Question")
        verbose_name_plural = _("Questions")

    def __str__(self):
        return "ID: " + str(self.id) + "; " + self.content[:50]

class Answer(models.Model):
    question = models.ForeignKey(Question, verbose_name=_("Question"), on_delete=models.CASCADE)

    content = MarkdownxField(blank=False,
                             help_text=_("Enter the answer text that "
                                         "you want displayed"),
                             verbose_name=_("Content"))

    correct = models.BooleanField(blank=False,
                                  default=False,
                                  help_text=_("Is this a correct answer?"),
                                  verbose_name=_("Correct"))

    def __str__(self):
        return self.content

    @property
    def get_content(self):
        return markdownify(self.content)

    class Meta:
        verbose_name = _("Answer")
        verbose_name_plural = _("Answers")


class Activity(models.Model):
    class ActivityType(models.TextChoices):
        PRACTICE_RESPONSE = 'P', "Practice Response"
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT, null=False, blank=False)
    activity_type = models.CharField(max_length=1, choices=ActivityType.choices, default=ActivityType.PRACTICE_RESPONSE)
    timestamp = models.DateTimeField()
    question = models.ForeignKey(Question, on_delete=models.PROTECT, null=False, blank=False)
    question_version = models.DateTimeField()
    quiz = models.ForeignKey(Quiz, on_delete=models.DO_NOTHING)
    answered_correctly = models.BooleanField(null=False, blank=False)
    answer_duration = models.DurationField(null=False, blank=False)
    saw_hint = models.BooleanField(null=False, blank=False)
    hint_duration = models.DurationField(null=False, blank=False)
    session = models.ForeignKey(Session, on_delete=models.DO_NOTHING)
    answer_list = ArrayField(models.IntegerField(), verbose_name=_("Answers Selected"), null=False, blank=True, default= list)
    answer_text = models.CharField(
        max_length=8, null=False, blank=True,
        default='', validators=[validate_unicode_slug],
        verbose_name=_("Correct Answer Text"))


class Course(models.Model):
    title = models.CharField(
        verbose_name=_("Title"),
        max_length=60, blank=False)

    description = models.TextField(
        verbose_name=_("Description"),
        blank=True, help_text=_("a description of the course"))

    group = models.OneToOneField(
        Group,
        blank=True,
        help_text=_('the Django auth group for this course, it maps to a subscription plan'),
        null=True,
        on_delete=models.SET_NULL,
    )

    active = models.BooleanField(
        blank=True, default=False,
        verbose_name=_("Active"),
        help_text=_("If no, the course is not displayed"
                    " in the available courses list"))

    url = models.SlugField(
        max_length=60, blank=False,
        help_text=_("a user friendly url"),
        verbose_name=_("user friendly url"))

    quiz = models.ManyToManyField(Quiz,
                                  verbose_name=_("Quiz"),
                                  blank=True)