from import_export import resources
from .models import Container, Quiz, Category, Question, Answer, Course, Progress


class AnswerResource(resources.ModelResource):
    class Meta:
        model = Answer

class ContainerResource(resources.ModelResource):
    class Meta:
        model = Container

class CourseResource(resources.ModelResource):
    class Meta:
        model = Course


class ProgressResource(resources.ModelResource):
    class Meta:
        model = Progress

class QuestionResource(resources.ModelResource):
    class Meta:
        model = Question


class QuizResource(resources.ModelResource):
    class Meta:
        model = Quiz


class CategoryResource(resources.ModelResource):
    class Meta:
        model = Category