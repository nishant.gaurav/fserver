from django import forms
from django.contrib import admin
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.utils.translation import ugettext_lazy as _
from import_export.admin import ImportExportModelAdmin
from django.urls import reverse
from django.utils.html import mark_safe

from .resources import *
from .models import Quiz, Category, SubCategory, Progress, Question, Answer
from quiz import models


class AnswerInline(admin.TabularInline):
    model = Answer
    extra = 4

class QuizAdminForm(forms.ModelForm):
    """
    below is from
    http://stackoverflow.com/questions/11657682/
    django-admin-interface-using-horizontal-filter-with-
    inline-manytomany-field
    """

    class Meta:
        model = Quiz
        exclude = []

    containers = forms.ModelMultipleChoiceField(
        queryset=Container.objects.all(),
        required=False,
        label=_("Containers"),
        widget=FilteredSelectMultiple(
            verbose_name=_("Containers"),
            is_stacked=False))

    prerequisites = forms.ModelMultipleChoiceField(
        queryset=Quiz.objects.all(),
        required=False,
        label=_("Prerequisites"),
        widget=FilteredSelectMultiple(
            verbose_name=_("Prerequisites"),
            is_stacked=False))

    def __init__(self, *args, **kwargs):
        super(QuizAdminForm, self).__init__(*args, **kwargs)
        if self.instance.pk:
            self.fields['containers'].initial =\
                self.instance.container_set.all()

    def save(self, commit=True):
        quiz = super(QuizAdminForm, self).save(commit=False)
        quiz.save()
        quiz.container_set.set(self.cleaned_data['containers'])
        self.save_m2m()
        return quiz


class QuizAdmin(ImportExportModelAdmin):
    form = QuizAdminForm
    resource_class = QuizResource

    list_display = ('title', 'category', )
    list_filter = ('category',)
    search_fields = ('description', 'category', )


class CategoryAdmin(ImportExportModelAdmin):
    search_fields = ('category', )
    resource_class = CategoryResource


class SubCategoryAdmin(admin.ModelAdmin):
    search_fields = ('sub_category', )
    list_display = ('sub_category', 'category',)
    list_filter = ('category',)

class ContainerAdmin(ImportExportModelAdmin):
    def model_str(self, obj):
        link = reverse("admin:quiz_question_changelist")
        return mark_safe(f'<a href="{link}?{obj._meta.model_name}__id__exact={obj.id}">Questions List</a>')
    model_str.short_description = 'Question'
    fields = ('quiz', 'paragraph', 'category', 'sub_category',)
    search_fields = ('paragraph',)
    list_filter = ('category', 'sub_category','quiz',)
    resource_class = ContainerResource
    filter_horizontal = ('quiz',)
    list_display = ('__str__', 'model_str', 'category', 'paragraph')

class QuestionAdmin(ImportExportModelAdmin):
    list_display = ('content', 'category', 'sub_category', 'id', 'level')
    resource_class = QuestionResource
    fields = ('content', 'container', 'tags',
              'hint', 'explanation', 'instructions', 'question_help',
              'level', 'question_type', 'answer_text', 'answer_order')

    search_fields = ('content', 'hint', 'explanation')

    inlines = [AnswerInline]


class ProgressAdmin(ImportExportModelAdmin):
    """
    to do:
            create a user section
    """
    search_fields = ('user', 'score', )
    resource_class = ProgressResource

class AnswerAdmin(ImportExportModelAdmin):
    resource_class = AnswerResource

class CourseAdmin(ImportExportModelAdmin):
    filter_horizontal = ('quiz',)
    list_display = ('id', 'title', 'group')
    resource_class = CourseResource

admin.site.register(Answer, AnswerAdmin)
admin.site.register(Quiz, QuizAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(SubCategory, SubCategoryAdmin)
admin.site.register(Container, ContainerAdmin)
admin.site.register(Question, QuestionAdmin)
admin.site.register(Progress, ProgressAdmin)
admin.site.register(Course, CourseAdmin)