import json
from django.shortcuts import get_object_or_404
from django.db.models import Case, When

from . import models
from . import serializers

from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status, permissions

class SubscriberPermissions(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return request.user.groups.filter(name = obj.group).exists()
        return False

class LoadQuizes(APIView):
    permission_classes = (permissions.IsAuthenticated, SubscriberPermissions,)

    def get(self, request, course):
        course = get_object_or_404(models.Course, url=course)
        self.check_object_permissions(request, course)
        quizes = models.Quiz.objects.filter(draft=False, course=course)
        quizes_data = serializers.QuizListSerializer(quizes).data
        return Response(quizes_data)

class LoadQuiz(APIView):

    def get(self, request, course, slug):
        # course = get_object_or_404(models.Course, url=course)
        # self.check_object_permissions(request, course)
        quiz = get_object_or_404(models.Quiz,url=slug)
        # TODO: if course does not contain quiz, Response(status=status.HTTP_400_BAD_REQUEST)
        session = models.Session.objects.user_session(user=request.user, quiz=quiz)
        preserved = Case(*[When(id=id, then=pos) for pos, id in enumerate(session.questions)])
        questions = quiz.get_questions().filter(id__in=session.questions).order_by(preserved)
        questions_data = serializers.QuestionListSerializer(questions).data
        quiz_data = serializers.QuizSerializer(quiz).data
        return Response({"quiz":quiz_data, "questions": questions_data})


class SaveActivity(APIView):

    def post(self, request):
        session = models.Session.objects.get(id=request.data["session"])
        question = models.Question.objects.get(id=request.data["question"])
        temp = request.data
        temp["user"] = request.user.id
        temp["question_version"] = str(question.updated_at)
        temp["quiz"] = session.quiz.id
        serialized = serializers.ActivitySerializer(data=temp)
        if serialized.is_valid():
            serialized.save()
            return Response(status=status.HTTP_202_ACCEPTED)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)
