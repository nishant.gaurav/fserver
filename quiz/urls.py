from django.conf.urls import url

from . import views

app_name = 'quiz'
urlpatterns = [
    url(r'^course/(?P<course>[\w-]+)/$', views.LoadQuizes.as_view(), name='quizes'),
    url(r'^course/(?P<course>[\w-]+)/(?P<slug>[\w-]+)/$', views.LoadQuiz.as_view(), name='quiz'),
    url(r'^activity/$', views.SaveActivity.as_view(), name='save_activity'),
]
