from rest_framework import serializers

from quiz import models

class QuizSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Quiz
        fields = '__all__'

class QuizListSerializer(serializers.ListSerializer):
    child = QuizSerializer()
    many = True
    allow_null = True

class ActivitySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Activity
        fields = '__all__'

class AnswerSerializer(serializers.ModelSerializer):
    content = serializers.CharField(source='get_content', read_only=True)
    class Meta:
        model = models.Answer
        fields = ('content','correct')

class ContainerSerializer(serializers.ModelSerializer):
    paragraph = serializers.CharField(source='get_paragraph', read_only=True)
    class Meta:
        model = models.Container
        fields = '__all__'

class QuestionSerializer(serializers.ModelSerializer):
    content = serializers.CharField(source='get_content', read_only=True)
    answers = AnswerSerializer(source='answer_set', many=True, read_only=True)
    hint = serializers.CharField(source='get_hint', read_only=True)
    explanation = serializers.CharField(source='get_explanation', read_only=True)
    class Meta:
        model = models.Question
        fields = '__all__'

class QuestionListSerializer(serializers.ListSerializer):
    child = QuestionSerializer()
    many = True
    allow_null = True

class CourseSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Course
        fields = '__all__'

class CourseListSerializer(serializers.ListSerializer):
    child = CourseSerializer()
    many = True
    allow_null = True
